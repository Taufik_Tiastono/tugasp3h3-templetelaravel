@extends('layout.master') 
@section('judul') Update Profile
@endsection
 
@section('judul1') update
@endsection
 @push('script')
<script src="https://cdn.tiny.cloud/1/dqywznnnbbirwxo9qbj9gm9ic6ejgdveunl4s53fwyu4n56j/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      //plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      //toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      plugins : 'advlist autolink lists link image charmap print priview hr anchor pageblank',
      toolbar_mode: 'floating',
      //tinycomments_mode: 'embedded',
      //tinycomments_author: 'Author name',
      
    });

</script>



@endpush 
@section('content')

<form action="/profile/{{$profile->id}}" method="POST">
    @csrf @method('PUT')

    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" value="{{$profile->user->name}}" disabled>
    </div>

    <div class="form-group">
        <label>Email</label>
        <input type="email" class="form-control" value="{{$profile->user->email}}" disabled>
    </div>

    <div class="form-group">
        <label>Umur</label>
        <input type="number" name="umur" class="form-control" value="{{$profile->umur}}" id="#">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" id="#">{{$profile->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" class="form-control" id="#">{{$profile->alamat}}</textarea>
    </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-warning">Update</button>
</form>
@endsection