<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="{{asset('adminLTE/index3.html')}}" class="brand-link">
      <img src="{{asset('adminLTE/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{asset('adminLTE/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        @guest
        <a href="#" class="d-block">Belum Login</a> @endguest @auth
        <a href="#" class="d-block">{{ Auth::user()->name }} ({{Auth::user()->profile->umur}})</a> @endauth
      </div>
    </div>



    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="/film" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Film
                {{-- <i class="right fas fa-angle-left"></i> --}}
              </p>
            </a> @auth
          <li class="nav-item">
            <a href="/genre" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Genre
                {{-- <i class="right fas fa-angle-left"></i> --}}
              </p>
            </a>
          </li>
          @endauth

          <li class="nav-item">
            <a href="/cast" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard Daftar Cast
                {{-- <i class="right fas fa-angle-left"></i> --}}
              </p>
            </a>

          </li>
          {{-- --}}
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Tables
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/table" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Simple Tables</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/data-tables" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>DataTables</p>
                </a>
              </li>

            </ul>
          </li>

          @auth
          <li class="nav-item">
            <a href="/profile" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Profile
              </p>
            </a>
          </li>


          <li class="nav-item">
            <a class="nav-link bg-danger" href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                {{-- {{ __('Logout') }} --}}
                <i class="nav-icon fa far fa-sign-in"></i>
                <p>Logout</p>
              </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
            </form>
            {{-- <i class="nav-icon fas fa-circle"></i> --}}


            </a>
          </li>
          @endauth @guest
          <li class="nav-item">
            <a href="/login" class="nav-link bg-success">
                <i class="nav-icon far fa-sign-in"></i>
                <p>
                  Login
                </p>
              </a>
          </li>
          @endguest

      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>