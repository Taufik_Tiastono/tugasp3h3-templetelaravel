@extends('layout.master') 
@section('judul') Detail {{$genre->nama}}
@endsection
 
@section('judul1') Genre
@endsection
 
@section('content')

<h3>List Film {{$genre->nama}}</h3>

<div class="row">
    {{-- karena listnya lebih dari satu menggunakan foreach --}} @foreach($genre->film as $item)
    <div class="col-md-3 col-sm-6 mb-3">
        <div class="card" style="width: 14rem;">
            <img src="{{asset('poster/'.$item->poster)}}" class="card-img-top" alt="...">
            <div class="card-body">
                {{-- <span class="badge badge-secondary">{{$item->genre->nama}}</span> --}}
                <h5 class="text-bold">{{$item->judul}} ({{$item->tahun}})</h5>
                <p class="card-text">{{ Str::limit($item->ringkasan, 100,'') }} <a href="/film/{{$item->id}}">Read more...</a></p>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection