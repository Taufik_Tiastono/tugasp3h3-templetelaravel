@extends('layout.master')

@section('judul')
Halaman Edit Genre {{$genre->nama}}
@endsection

@section('judul1')
Edit
@endsection

@section('content')
<form action="/genre/{{$genre->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama Genre</label>
    <input type="text" name="nama" value="{{$genre->nama}}"class="form-control" id="#" >
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-warning">Update</button>
</form>
@endsection