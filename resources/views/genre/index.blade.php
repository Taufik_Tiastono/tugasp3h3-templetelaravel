@extends('layout.master') 
@section('judul') Daftar Genre
@endsection
 
@section('judul1') Genre
@endsection
 
@section('content')
<a href="/genre/create" class="btn btn-success btn-md mb-3">Tambah Data</a>

<table class="table table-hover table-striped">
    {{--
    <div class="container-fluid"> --}}
        <thead class="thead-dark">
            <tr class="row">
                <th class="col">#</th>
                <th class="col">Nama Genre</th>
                <th class="col text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($genre as $key => $item)
            <tr class="row">
                <td class="col">{{$key +1}}</td>
                <td class="col">{{$item->nama}}</td>
                <td class="col text-center">
                    <form action="/genre/{{$item->id}}" method="POST">
                        <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a> @method('delete') @csrf
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
            @empty
            <tr class="row">
                <td class="col">Data Masih Kosong</td>
            </tr>
            @endforelse
        </tbody>
    </div>
</table>
@endsection