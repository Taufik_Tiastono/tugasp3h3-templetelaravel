@extends('layout.master')

@section('judul')
Detail Cast  {{$cast->nama}}
@endsection

@section('judul1')
Detail
@endsection

@section('content')
<h3>{{$cast->nama}}</h3>

<p>{{$cast->umur}} tahun</p>

<p>{{$cast->bio}}</p>
@endsection