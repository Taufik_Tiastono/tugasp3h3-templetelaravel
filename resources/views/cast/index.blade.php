@extends('layout.master')

@section('judul')
Daftar Cast
@endsection

@section('judul1')
Cast
@endsection


@section('content')

<a href="/cast/create" class="btn btn-success btn-md mb-3">Tambah Data</a>

<table class="table table-hover table-striped">
{{-- <div class="container-fluid"> --}}
    <thead class="thead-dark" >
      <tr class="row">
        <th class="col-1">#</th>
        <th class="col-2">Nama</th>
        <th class="col-1">Umur</th>
        <th class="col-5">Bio</th>
        <th class="col-3 text-center">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $item)
        <tr class="row"> 
            <td class="col-1">{{$key +1}}</td>
            <td class="col-2">{{$item->nama}}</td>
            <td class="col-1">{{$item->umur}}</td>
            <td class="col-5">{{$item->bio}}</td>
            <td class="col-3 text-center">
                <form action="/cast/{{$item->id}}" method="POST">
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @method('delete')
                    @csrf
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
        </tr>    
      @empty
      <tr class="row">
          <td class="col">Data Masih Kosong</td>
      </tr>
      @endforelse    
    </tbody>
</div> 
</table>

@endsection