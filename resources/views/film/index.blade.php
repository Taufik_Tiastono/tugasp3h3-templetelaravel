@extends('layout.master') 
@section('judul') List Film
@endsection
 
@section('judul1') film
@endsection
 
@section('content')
@auth
<a href="/film/create" class="btn btn-primary mb-3">Tambah Film</a> @endauth

<div class="row">
    @forelse($film as $item)
    <div class="col-md-3 col-sm-6 mb-3">
        <div class="card" style="width: 14rem;">
            <img src="{{asset('poster/'.$item->poster)}}" class="card-img-top" alt="...">
            <div class="card-body">
                <span class="badge badge-secondary">{{$item->genre->nama}}</span>
                <h5 class="text-bold">{{$item->judul}} ({{$item->tahun}})</h5>
                {{-- @foreach ($genre as $g) @if($item->genre_id === $g->id)
                <p>Genre : {{$g->nama}}</p>
                @else @endif @endforeach --}} {{--
                <p class="card-text">{{ Str::limit($item->ringkasan, 100, $end = ' ...') }}</p> --}}
                <p class="card-text">{{ Str::limit($item->ringkasan, 100,'') }} <a href="/film/{{$item->id}}">Read more...</a></p>
                @auth
                <form action="/film/{{$item->id}}" method="POST">
                    @csrf @method('delete')
                    <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete"></a>
                </form>
                @endauth @guest
                <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a> @endguest
            </div>
        </div>
    </div>

    @empty
    <h1>Data Film Masih Kosong</h1>
    @endforelse


</div>
@endsection