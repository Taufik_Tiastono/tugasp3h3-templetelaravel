@extends('layout.master') 
@section('judul') Detail Film {{$film->judul}}
@endsection
 
@section('judul1') Film
@endsection


@section('content')

<div class="card mb-3">
    <div class="row no-gutters">
        <div class="col-md-3">
            <img src="{{asset('poster/'.$film->poster)}}" class="mx-auto" style="width: 15rem;" alt="">
        </div>
        <div class="col-md-9">
            <div class="card-body">
                <span class="badge badge-secondary">{{$film->genre->nama}}</span>
                <h4>{{$film->judul}} {{$film->tahun}}</h5>
                    <p>{{$film->ringkasan}}</p>
            </div>
        </div>
    </div>
</div>

<h2>Komentar</h2>

@forelse($film->kritik as $item)
<div class="card">
    <div class="card-body">

        <h6><i class="fa fa-star" style="color:gold"> </i> {{$item->point}} /10</h6>
        <h6>{{$item->isi}}</h6>
        <small><b>{{$item->user->name}}</b></small> {{-- <a href="#" class="card-link">Card link</a>
        <a href="#" class="card-link">Another link</a> --}}
    </div>
</div>
@empty
<h4>Tidak ada Komentar </h4>
@endforelse

<form action="/kritik" method="POST">
    @csrf
    <div class="form-group">
        <label>Tinggalkan komentarmu</label> {{-- <input type="hidden" value="{{$user->id}}" name="user_id"> --}}
        <input type="hidden" value="{{$film->id}}" name="film_id">

        <textarea name="isi" class="form-control"></textarea>
    </div>
    @error('isi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Berikan Penilaian 1 s.d 10</label>
        <input type="number" name="point" class="form-control" id="#">
    </div>
    @error('point')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection