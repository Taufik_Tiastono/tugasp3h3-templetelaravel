<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Film;
use File;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //middleware
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);

        /* $this->middleware('log')->only('index');

        $this->middleware('subscribed')->except('store'); */
    }

    public function index()
    {
        //tes kalo sukses
        //return "sukses";
        $genre = DB::table('genre')->get();
        $film = Film::all();
        return view('film.index', compact('film','genre'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        //memasukkan list genre
        $genre = DB::table('genre')->get();
        return view('film.create', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg',
            'genre_id' => 'required',
        ]);
        
        // ubah mengambil nama gambar ubah namanya dengan waktu, 
        // lalu simpan gambar di folder public/poster/..
        $namaPoster = time().'.'.$request->poster->extension();  
        $request->poster->move(public_path('poster'), $namaPoster);

        $film = new Film;

        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->poster = $namaPoster;
        $film->genre_id = $request->genre_id;

        $film->save();
        return redirect('/film');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $genre = DB::table('genre')->get();
        $film = Film::findOrFail($id);
        return view('film.show', compact('film','genre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $genre = DB::table('genre')->get();
        $film = Film::findOrFail($id);
        return view('film.edit', compact('film','genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'image|mimes:jpeg,png,jpg',
            'genre_id' => 'required',
        ]);

        if($request->has('poster')){
            $film = Film::find($id);

            //hapus file poster yg lama
            $path = "poster/";
            File::delete($path . $film->poster);

            $namaPoster = time().'.'.$request->poster->extension();  
            $request->poster->move(public_path('poster'), $namaPoster);

            $film->judul = $request->judul;
            $film->ringkasan = $request->ringkasan;
            $film->tahun = $request->tahun;
            $film->poster = $namaPoster;
            $film->genre_id = $request->genre_id;

            $film->save();

            return redirect('/film');

        }else{
            $film = Film::find($id);

            $film->judul = $request->judul;
            $film->ringkasan = $request->ringkasan;
            $film->tahun = $request->tahun;

            $film->genre_id = $request->genre_id;

            $film->save();

            return redirect('/film');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $film = Film::find($id);

            //hapus file poster yg lama
        $path = "poster/";
        File::delete($path . $film->poster);

        $film->delete();

        return redirect('/film');
    }
}
