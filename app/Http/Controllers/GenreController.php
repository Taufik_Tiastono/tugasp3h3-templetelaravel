<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Genre;
use RealRashid\SweetAlert\Facades\Alert;

class GenreController extends Controller
{
    //
    public function create(){
        return view('genre.create');
    }

    //function untuk save ke DB
    public function store(Request $request){
        $request->validate([
            'nama' => 'required'
        ]);

        DB::table('genre')->insert([
            'nama' => $request['nama'],
        ]);

        Alert::success('Berhasil', 'Tambah Data Genre Berhasil');
        return redirect('/genre');
    }

    //function menampilkan data DB
    public function index(){
        //query builder
        // $genre = DB::table('genre')->get();
        
        //ORM
        $genre = Genre::all();
        return view('genre.index', compact('genre'));
       
    }

    //function menampilkan detail data
    public function show($id){
        // query builder
        // $genre = DB::table('genre')->where('id' , $id)->first();
        
        //ORM
        $genre = Genre::find($id);
        return view('genre.show', compact('genre'));
    }

    //function edit
    public function edit($id){
        $genre = DB::table('genre')->where('id' , $id)->first();
        return view('genre.edit', compact('genre'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required'
        ]);

        $query = DB::table('genre')
              ->where('id', $id)
              ->update([
                  'nama' => $request['nama'],
                ]);    

        Alert::success('Berhasil', 'Update Data Genre Berhasil');        
        return redirect('/genre');        

    }

    public function destroy($id){
        DB::table('genre')->where('id', $id)->delete();
        Alert::success('Delete', 'Data Genre Berhasil di Hapus');
        return redirect('/genre');
    }
}
