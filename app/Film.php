<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    //
    protected $table = 'film';
    protected $fillable = ['judul','ringkasan','tahun','poster','genre_id'];

    //relasi one to many
    public function genre()
    {
        return $this->belongsTo('App\Genre');
    }

    public function kritik()
    {
        return $this->hasMany('App\Kritik');
    }
}
