<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



// Route::get('/', function () {
//     return view('layout.index');
// });

Route::get('/master', function () {
    return view('layout.master');
});


Route::get('/table',function(){
    return view('layout.tabel');
});

Route::get('/data-tables',function(){
    return view('layout.datatabel');
});

//CRUD Cast
Route::get('/cast/create' , 'CastController@create');

Route::post('/cast' , 'CastController@store');


//Read
Route::get('/cast', 'CastController@index');

//detail
Route::get('/cast/{cast_id}', 'CastController@show');

//edit
Route::get('/cast/{cast_id}/edit', 'CastController@edit');

Route::put('/cast/{cast_id}', 'CastController@update');

//delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');

//Buat Middleware, kalo belum login page nya gak bisa diakses
Route::group(['middleware' => ['auth']], function () {
    
    //CRUD Genre QueryBuilder
    Route::get('/genre/create' , 'GenreController@create');
    Route::post('/genre' , 'GenreController@store');
    //Read
    Route::get('/genre', 'GenreController@index');
    //detail
    Route::get('/genre/{genre_id}', 'GenreController@show');
    //edit
    Route::get('/genre/{genre_id}/edit', 'GenreController@edit');
    Route::put('/genre/{genre_id}', 'GenreController@update');
    //delete
    Route::delete('/genre/{genre_id}', 'GenreController@destroy');

    //Update Profile
    Route::resource('profile','ProfileController')->only([
        'index','update'
    ]);

    Route::resource('kritik','KritikController')->only([
        'store']);

});



//CRUD FILM ORM Cara Cepat
Route::resource('film','FilmController');


//Authentifikasi
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
